//import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:todo_list/widgets/new_task.dart';
import 'package:todo_list/widgets/task_list.dart';
import 'package:todo_list/models/tasks.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'ToDo App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final List<Tasks> _tasks = [
    // Tasks(
    //   title: 'Complete module 7',
    //   date: DateTime.now(),
    //   id: 't1',
    // ),
    // Tasks(
    //   title: 'Buy groceries',
    //   date: DateTime.now(),
    //   id: 't2',
    // ),
  ];

  void _addNewTask(String taskTitle, DateTime chosenDate) {
    final newTx = Tasks(
        title: taskTitle, date: chosenDate, id: DateTime.now().toString());

    setState(() {
      _tasks.add(newTx);
    });
  }

  void _deleteTasks(String id) {
    setState(() {
      _tasks.removeWhere((element) => element.id == id);
    });
  }

  bool _showList = false;

  @override
  Widget build(BuildContext context) {
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const Center(
          child: Text('My Tasks'),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text('View More'),
              Switch.adaptive(
                activeColor: Colors.amber,
                value: _showList,
                onChanged: ((value) {
                  setState(() {
                    _showList = value;
                  });
                }),
              ),
            ],
          ),
          if (isPortrait)
            SizedBox(
              child: NewTask(_addNewTask, _tasks),
            ),
          Card(
            elevation: 5,
            child: TaskList(_tasks, _deleteTasks),
          ),
          if (!isPortrait)
            SizedBox(
              child: NewTask(_addNewTask, _tasks),
            ),
        ],
      ),
    );
  }
}
