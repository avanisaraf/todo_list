class Tasks {
  final String id;
  final String title;
  final DateTime date;

  Tasks({required this.title, required this.date, required this.id});
}
