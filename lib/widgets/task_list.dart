import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:todo_list/models/tasks.dart';

class TaskList extends StatefulWidget {
  final List<Tasks> inputTasks;
  final Function deleteTx;

  // ignore: use_key_in_widget_constructors
  const TaskList(this.inputTasks, this.deleteTx);

  @override
  State<TaskList> createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 350,
      child: ListView.builder(
        itemCount: widget.inputTasks.length,
        itemBuilder: ((context, index) {
          return ListTile(
            title: Text(widget.inputTasks[index].title),
            subtitle: Text(
              DateFormat.yMd().format(widget.inputTasks[index].date),
            ),
            trailing: IconButton(
              onPressed: (() {
                widget.deleteTx(widget.inputTasks[index].id);
              }),
              icon: const Icon(Icons.delete),
              color: Theme.of(context).errorColor,
            ),
          );
        }),
      ),
    );
  }
}
