import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:todo_list/models/tasks.dart';

class NewTask extends StatefulWidget {
  final Function addTask;
  final List<Tasks> tasks;
  const NewTask(this.addTask, this.tasks, {Key? key}) : super(key: key);

  @override
  State<NewTask> createState() => _NewTaskState();
}

class _NewTaskState extends State<NewTask> {
  final titleController = TextEditingController();
  DateTime? selectedDate;

  void presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2021),
      lastDate: DateTime(2023),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }

      setState(() {
        selectedDate = pickedDate;
      });
    });
  }

  void submitData() {
    final enteredText = titleController;
    setState(() {
      widget.addTask(enteredText, selectedDate);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        margin: const EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 10,
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.end, children: [
          ListTile(
              title: TextField(
                decoration: const InputDecoration(labelText: 'Title'),
                controller: titleController,
                onSubmitted: (_) => submitData(),
              ),
              subtitle: Row(
                children: [
                  const Text(
                    'Due Date : ',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15,
                    ),
                  ),
                  IconButton(
                    onPressed: presentDatePicker,
                    icon: const Icon(Icons.calendar_month),
                    color: Colors.blue,
                  ),
                  Text(
                    DateFormat.yMd().format(selectedDate ?? DateTime.now()),
                  ),
                ],
              )),
          ElevatedButton(
            onPressed: () {
              setState(() {
                if (titleController.text.isNotEmpty) {
                  widget.tasks.add(
                    Tasks(
                      title: titleController.text,
                      date: selectedDate!,
                      id: DateTime.now().toString(),
                    ),
                  );
                }
              });
            },
            child: const Text('Submit'),
          ),
        ]),
      ),
    );
  }
}
